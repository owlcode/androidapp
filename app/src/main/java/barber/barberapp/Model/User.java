package barber.barberapp.Model;

import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;

public class User {
    private static final String[] CREDS = {"1:user:pass:1", "2:boss:pass:0", "3:dawid:dawid:1", "4:a:a:0"};
    private static final Set<String> CREDS_TREE = new TreeSet<>(Arrays.asList(CREDS));


    public static String userExist(String username, String password) {
        String[] user;
        for (String str : CREDS_TREE) {
            user = str.split("\\:");
            if (user[1].equals(username) && user[2].equals(password)) {
                return user[0];
            }
        }
        return "0";
    }

    /* returns authorization Level ("0" for boss, 1 for a normal user) or "2" if user isn't found */
    public static String checkPrivileges(String userId) {
        String[] user;
        for (String s : CREDS_TREE) {
            user = s.split("\\:");
            if (user[0].equals(userId)) {
                return user[3];
            }
        }
        return "2";
    }
}


