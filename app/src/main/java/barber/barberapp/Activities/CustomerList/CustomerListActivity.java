package barber.barberapp.Activities.CustomerList;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.ListView;
import barber.barberapp.Activities.Sidebar;
import barber.barberapp.Model.Customer;
import barber.barberapp.R;
import java.util.List;
import android.app.AlertDialog;
import android.content.DialogInterface;

public class CustomerListActivity extends Sidebar {
    List<Customer> list = null;

    private ListView customerList;
    private ClientAdapter adapter;
    private SearchView searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        LayoutInflater inflater = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.customer_list, null, false);
        drawer.addView(contentView, 0);

//        setContentView(R.layout.customer_list);
        customerList = (ListView)findViewById(R.id.listView);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        adapter = new ClientAdapter( getApplicationContext(), R.layout.customer_list_row );
        customerList.setAdapter(adapter);

        searchView = (SearchView)findViewById(R.id.searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                callSearch(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
//              if (searchView.isExpanded() && TextUtils.isEmpty(newText)) {
                callSearch(newText);
//              }
                return true;
            }

            public void callSearch(String query) {
                adapter.getFilter().filter(query);
            }
        });
    }

    public void deleteCustomers( View view ) {
        AlertDialog.Builder builder = new AlertDialog.Builder( CustomerListActivity.this );
        builder.setPositiveButton("Tak", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which)
            {
                Customer.clearAll( getApplicationContext() );
                Intent intent = getIntent();
                finish();
                startActivity(intent);
            }
        });
        builder.setNegativeButton("Nie", new DialogInterface.OnClickListener()
        {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        builder.setMessage("Czy na pewno chcesz usunąć listę klientów?");
        builder.setTitle("Uwaga!");

        AlertDialog d = builder.create();
        d.show();
    }

}
