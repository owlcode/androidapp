package barber.barberapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import barber.barberapp.Activities.CustomerList.CustomerListActivity;
import barber.barberapp.Model.Auth;
import barber.barberapp.Model.Customer;

import java.util.Random;

public class SignIn extends Activity {
    private static TextView mainText;
    private static EditText username;
    private static EditText password;
    private static String usernameStr = "";

    public static String getUsernameStr() {
        return usernameStr;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Auth.userLoggedIn(this.getApplicationContext())) {
            addClients();

            Intent cl = new Intent(getBaseContext(), CustomerListActivity.class);
            startActivity(cl);
            finish();
        } else {
            setContentView(R.layout.activity_sign_in);
        }

    }

    private void addClients() {
        Random generator = new Random();
        Customer c;
//
        String[] name = {"Katarzyna", "Anna", "Mariola", "Barbara", "Basia", "Kasia", "Aleksandra", "Xenia", "Małgorzata", "Ilona", "Joanna", "Asia", "Aneta", "Ela", "Ala", "Magda"};
        String[] surname = {"Kowalczyk", "Sowa", "Leszczyńska", "Mrozowska", "Jakubowska", "Szczepanowska", "Sawicka", "Lodyga", "Szczegółka", "Szczypior", "Ambandzja", "Goryl"};
        String[] client = new String[4];

        for (int i = 0; i < 1000; i++) {
            client[0] = name[generator.nextInt(15)];
            client[1] = surname[generator.nextInt(12)];
            client[2] = client[0].toLowerCase().charAt(0) + '.' + client[1] + "@gmail.com";
            client[3] = "48 123 45 67";

            c = new Customer(client);
            c.add(c, this.getApplicationContext());
        }
    }

    public void verifySignIn(View view) {
        username = (EditText) findViewById(R.id.editText);
        password = (EditText) findViewById(R.id.password);
        mainText = (TextView) findViewById(R.id.mainText);
        String passwordStr,
                userId,
                role;

        usernameStr = username.getText().toString();
        passwordStr = password.getText().toString();

        if (Auth.login(usernameStr, passwordStr, this.getApplicationContext())) {
            mainText.setText("Zalogowano");
            Intent cl = new Intent(getBaseContext(), CustomerListActivity.class);
            startActivity(cl);
            finish();
        } else {
            mainText.setText("Złe hasło");
        }

    }
}

















