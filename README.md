# Projekt Inżynieria Oprogramowania - BarberApp #

Aplikacja android służąca do katalogowania klientów i ich wizyt w salonie fryzjerskim.

### Prowadzący ###

* dr inż. Dominik Olszewski

### Zespół ###

* Dawid Sowa - prototypy modeli, organizacja
* Michał Łodyga - lista klientów, lista wizyt
* Jakub Mrozowski - zapisywanie danych
* Yauheni Savitsky - testy jednostkowe
* Paweł Szczepanowski - koncepcja, menu

### Changelog ###
 v0.5 - BarberApp Alfa